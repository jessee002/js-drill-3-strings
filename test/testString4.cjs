const String4 = require("../string4.cjs");

const obj = {
    first_name: "JoHN", 
    last_name: "SMith"
}

const exp_obj = {
    name : "JoHN",
    middle_name : "doe",
    last_name : "SMith"
}

const result = String4(obj,exp_obj);
console.log(result)