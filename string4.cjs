function capitalize(str){
    f_char = str.charAt(0).toUpperCase()
    remain_char = str.substring(1).toLowerCase();
    return f_char + remain_char;
}

function String4(obj,exp_obj)
{
    let str = "";
    if(typeof obj == 'object' && typeof exp_obj == 'object') //if(!obj) means null
    {
        for(let key in exp_obj)
        {
            if(obj[key] == undefined){
                str += capitalize(exp_obj[key]) + " ";
            }else{
                str += capitalize(obj[key]) + " ";
            }
        }
        return str;
    }
    else{
        return [];
    }
}
module.exports = String4;
