function String5(arr_str)
{
    let str = " ";
    if(Array.isArray(arr_str) && arr_str.length > 0) //if(!arr_str) means null
    {
        for(let ele in arr_str)
        {
            if(typeof arr_str[ele] == 'string'){
                str += arr_str[ele] + " ";
            }
        }
        return str
    }
    else{
        return " ";
    }
}
module.exports = String5;